# MSNwrite

_For the young adults who surfed the wave of the roaring dotcom bubble_

# Usage

Simply pipe a string to it.

```
$ echo yolo | msnwrite
:alphabet-white-y::alphabet-white-o::alphabet-white-l::alphabet-white-o:
```
