package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"regexp"
)

// Args contains the command line arguments
type Args struct {
	Color string
}

func main() {
	args := ParseArgs()

	info, err := os.Stdin.Stat()
	if err != nil {
		panic(err)
	}

	if info.Mode()&os.ModeCharDevice != 0 || info.Size() <= 0 {
		fmt.Println("The command is intended to work with pipes.")
		return
	}

	reader := bufio.NewReader(os.Stdin)
	var output []byte

	for {
		input, err := reader.ReadByte()
		if err != nil && err == io.EOF {
			break
		}
		if !IsAlphanumeric(string(input)) {
			output = append(output, input)
			continue
		}
		MSNchar := []byte(fmt.Sprintf(":alphabet-%s-%s:", args.Color, string(input)))
		output = append(output, MSNchar...)
	}
	fmt.Printf(string(output))
}

// IsAlphanumeric checks if the input is an alphanumeric character
func IsAlphanumeric(input string) bool {
	isAlphanumericRE := regexp.MustCompile(`[a-zA-Z]`).MatchString
	return isAlphanumericRE(input)
}

// ParseArgs parses the command line arguments
func ParseArgs() Args {
	var args Args

	flag.StringVar(&args.Color, "color", "white", "the color of the font. can be white (default) or yellow")

	flag.Usage = func() {
		fmt.Printf("Converts a string into MSN style emojis for Slack")
		fmt.Printf("Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
	}

	flag.Parse()
	if !(args.Color == "yellow" || args.Color == "white") {
		fmt.Println("Invalid color specified. You should use either yellow or white.")
		os.Exit(1)
	}
	return args
}
